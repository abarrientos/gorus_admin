'use strict'

/*
|--------------------------------------------------------------------------
| Routes
  Existen 2 grupos de rutas: Las que necesitan autenticación del usuario y las que no. TOMAR MUY EN CUENTA ESTO
  Dentro del grupo de las que necesitan autenticación se debe especificar el middleware para verificar si el
  usuario(autenticado) tiene el permiso para acceder a esa ruta. Ejemplo: .middleware('verifyPermission:1')
  El número 1 indica el permiso que debe tener para acceder a esa ruta
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return {
    greeting: 'Hello world in JSON'
  }
})

const addPrefixToGroup = group => {
  // Grupo para rutas con prefijo /api/
  group.prefix("api");
  return group;
};


addPrefixToGroup(
  Route.group(() => {
    // Insertar rutas sin protección de autenticación aquíkk
    Route.post("login", "UserController.login") // metodo de logueo
    Route.post("register", "UserController.register_user") //registro user
    Route.post("register_play", "UserController.register_userPlay") //registro userplay


    Route.get('perfil_img/:file', 'UploadController.getFileByDirectoryPerfil')
    Route.get('profile_img/:file', 'UploadController.getFileByDirectoryProfile')
    Route.get('portada_img/:file', 'UploadController.getFileByDirectoryPortada')
    Route.get('logro_img/:file', 'UploadController.getFileByDirectoryLogros')
    Route.get("category_img/:file", "UploadController.getFileByDirectoryCategory")
    Route.get("producto_file/:file/:index/:type", "UploadController.getFileByDirectoryProducto")
    Route.get("producto_imgMain/:file", "UploadController.getFileByDirectoryProductoMain")
    Route.get("publicidad_img/:file/:index", "UploadController.getFileByDirectoryPublicidad")
    Route.get("publicacion_img/:file/:index", "UploadController.getFileByDirectoryPublicacion")

  })
);

addPrefixToGroup(
  Route.group(() => {
    // Insertar rutas con protección de autenticación aquí


    // rutas de los usuarios (usuarios) ///////////////////////////////
    Route.resource("user", "UserController").apiOnly
    Route.post("setToken", "UserController.setToken");
    Route.get("indexChannel/:id", "UserController.indexChannel");
    /////////////////////////////////////////////////////////////////////

    // rutas de las categorias (categories) ///////////////////////////////
    Route.resource("categories", "CategoryController").apiOnly
    /////////////////////////////////////////////////////////////////////

    // rutas de las Subcategorias (subcategories) ///////////////////////////////
    Route.resource("subcategories", "SubcategoryController").apiOnly
    /////////////////////////////////////////////////////////////////////

    // rutas de los productos (productos) ///////////////////////////////
    Route.resource("producto", "ProductoController").apiOnly
    Route.put("producto_blocked/:id", "ProductoController.updateBlocked")
    Route.get("indexByContentType/:id", "ProductoController.indexByContentType")
    Route.get("indexByContentTypeAndId/:id/:user", "ProductoController.indexByContentTypeAndId")
    Route.put("product_edit_img/:id", "ProductoController.productImgEdit")
    Route.get("getProducts", "ProductoController.getProducts")
    /////////////////////////////////////////////////////////////////////

    // rutas de la publicidad (publicidad) ///////////////////////////////
    Route.resource("publicidad", "PublicidadController").apiOnly
    Route.put("publicidad_edit_img/:id", "PublicidadController.publicImgEdit")
    /////////////////////////////////////////////////////////////////////

    // rutas de las publicaciones (publicaciones) ///////////////////////////////
    Route.resource("publicacion", "PublicacioneController").apiOnly
    Route.get("getPublicationsByUser/:id", "PublicacioneController.getPublicationsByUser")
    /////////////////////////////////////////////////////////////////////

    // rutas de los comentarios (comentarios) ///////////////////////
    Route.resource("comentario", "CommentController").apiOnly
    /////////////////////////////////////////////////////////////////
    
    // rutas de los retiros (retiros) ///////////////////////////////
    Route.resource("retiro", "WithdrawalController").apiOnly
    Route.get("getWithdrawals", "WithdrawalController.getWithdrawals")
    Route.get("changeStatus/:action/:id", "WithdrawalController.changeStatus")
    /////////////////////////////////////////////////////////////////


    Route.get("user_info", "UserController.getUserInfo") // obtiene la informacion del usuario logueado
    Route.get("getUserById/:id", "UserController.getUserById") // obtiene la informacion de un usuario por id
    Route.get("getUsersPlay", "UserController.getUsersPlay") // obtiene todos los usuarios play
    Route.post("uploadPortada", "UserController.setPortada")


    Route.post('chat/:id', 'ChatController.store')
    Route.get("show_all_chats", "ChatController.showAllChats")
    Route.get("show_all_messages/:id", "ChatController.showAllMessages")
    Route.post("send_message/:id", "ChatController.sendMessage") // metod para enviar un mensaje
    Route.put("messageSeen/:id", "ChatController.messageSeen")
    Route.put("updateChat/:id", "ChatController.updateChat")

  }).middleware("auth")
)
