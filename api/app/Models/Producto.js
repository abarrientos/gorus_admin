'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Producto extends Model {

  static fieldValidationRules() {
    const rules = {
      name: 'required',
      description: 'required',
      price: 'number',
      contentType: 'required',
      productType: 'required'
    }
    return rules
  }

  category () {
    return this.hasOne('App/Models/Category', 'category_id', '_id')
  }

  subcategory () {
    return this.hasOne('App/Models/Subcategory', 'subcategory_id', '_id')
  }
}

module.exports = Producto
