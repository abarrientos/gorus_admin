'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Publicacion extends Model {
  static fieldValidationRules() {
    const rules = {
      type: 'required',
      description: 'required',
      contentType: 'required'
    }
    return rules
    
  }
  user_info () {
    return this.hasOne('App/Models/User', 'user_id', '_id')
  }
  comments () {
    return this.hasMany('App/Models/Comment', '_id', 'publication_id')
  }
}

module.exports = Publicacion
