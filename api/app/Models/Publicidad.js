'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Publicidad extends Model {

  static fieldValidationRules() {
    const rules = {
      name: 'required'
    }
    return rules
  }
}

module.exports = Publicidad
