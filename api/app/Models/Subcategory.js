'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Subcategory extends Model {
  static fieldValidationRules() {
    const rules = {
      category_id: 'required',
      name: 'required'
    }
    return rules
  }

  category () {
    return this.hasOne('App/Models/Category', 'category_id', '_id')
  }
}

module.exports = Subcategory
