'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

class User extends Model {
  static get fillable() {
    return ['email', 'userName','password']
  }

  static get fillable_play() {
    return ['email', 'userName', 'password','description']
  }

  static fieldValidationRules_play() {
    const rulesUser = {
      userName: 'required',
      email: 'required|email',
      password: 'required|string|max:256',
      description: 'required|string|max:256'
    }
    return rulesUser
  }

  static fieldValidationRules() {
    const rulesUser = {
      userName: 'required',
      email: 'required|email',
      password: 'required|string|max:256'
    }
    return rulesUser
  }


  static boot() {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens() {
    return this.hasMany('App/Models/Token')
  }
}

module.exports = User
