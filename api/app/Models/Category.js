'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Category extends Model {

  static fieldValidationRules() {
    const rules = {
      name: 'required',
      description: 'required'
    }
    return rules
  }

}

module.exports = Category
