'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Comment extends Model {
  static fieldValidationRules() {
    const rules = {
      description: 'required',
      publication_id: 'required'
    }
    return rules
    
  }
  user_info () {
    return this.hasOne('App/Models/User', 'user_id', '_id')
  }
  publication () {
    return this.hasMany('App/Models/Publicacion', 'publication_id', '_id')
  }
}

module.exports = Comment
