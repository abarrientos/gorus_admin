'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Chat extends Model {
  data_gorus () {
    return this.hasOne('App/Models/User', 'gorus_id', '_id')
  }
  data_gorus_play () {
    return this.hasOne('App/Models/User', 'gorus_play_id', '_id')
  }
  lastMessage () {
    return this.hasOne('App/Models/Message', 'last_message_id', '_id')
  }
}

module.exports = Chat
