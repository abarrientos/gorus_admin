const Actions = use('App/Functions/Actions')

exports.index = async ({type, modelo, actions, parametros}) => {
  const Modelo = use(`App/Models/${modelo}`)
  let data = []
  let params = parametros ? parametros : {}
  if (type === 1) { // busqueda normal (de todos los registros o por parametros)
    data = (await Modelo.query().where(params).fetch()).toJSON()
  }

  if (actions.formName) {
    const actionsData = await Actions.actionsReturn(['edit', 'delete'], data, `${actions.formName}`)
    data = actionsData
  }
  console.log(data, 'returnData Crud.index')
  return data
}
