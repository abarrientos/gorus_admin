exports.actionsReturn = async (acciones, dataSend, ruta) => {
  for (let j of dataSend) {
    const actions = []
    for (let h of acciones) {
      if (h === 'show') { actions.push({ title: 'Ver', url: ruta + 'form/' + j._id, action: null, icon: 'bi-eye' }) }
      if (h === 'edit') { actions.push({ title: 'Editar', url: ruta + 'form/' + j._id, action: null, icon: 'bi-pencil-fill' }) }
      if (h === 'delete') { actions.push({ title: 'Eliminar', url: null, action: 'delete', icon: 'bi-trash-fill' }) }
    }
    j.actions = actions
  }
  return dataSend
}
