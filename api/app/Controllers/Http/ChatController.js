'use strict'
const Chat = use("App/Models/Chat")
const Message = use("App/Models/Message")
const User = use("App/Models/User")
const moment = require('moment')
var ObjectId = require('mongodb').ObjectId;
const Helpers = use('Helpers')
const mkdirp = use('mkdirp')
var randomize = require('randomatic');
const fs = require('fs')


/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with chats
 */
class ChatController {
  /**
   * Show a list of all consultations.
   * GET consultations
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new chat.
   * GET consultations/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new chat.
   * POST consultations
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ params, response, auth }) {
    const user = ((await auth.getUser()).toJSON())._id
    let body = {}
    body.gorus_id = new ObjectId(user)
    body.gorus_play_id = new ObjectId(params.id)
    body.isActive = true
    let hasPrevChat = true
    let chat = {}
    try {
      chat = (await Chat.query().where({gorus_id: body.gorus_id, gorus_play_id: body.gorus_play_id}).first()).toJSON()
    } catch (error) {
      hasPrevChat = false
      console.log('Nuevo chat')
    }
    if (hasPrevChat === false) {
      chat = (await Chat.create(body)).toJSON()
    }
    response.send(chat)
  }

  
  async updateChat ({ response, params, request }) {
    let body = request.all()
    const id = new ObjectId(params.id)
    let updatedChat = await Chat.query().where('_id', id).update(body)
    response.send(updatedChat)
  }

  async isNewMessages ({ params, response, auth }) {
    const user = (await auth.getUser()).toJSON()
    if (user.roles[0] === 2) {
      let send = {}
      let consultations = (await Chat.query().where('client_id', params.id).with('data_request').with('data_supplier').fetch()).toJSON()
      for (let i in consultations) {
        let request = (await Necesidad.query().find(consultations[i].request_id)).toJSON()
        if (request.status === 1 && request.isExtend === false) {
          send.quotationExtend = true
          send.idQuotationExtend = consultations[i]._id
        }
        if (request.status === 2 && request.isFinished === false) {
          send.quotationFinished = true
          send.idQuotationFinished = consultations[i]._id
          send.finished = consultations[i]
          send.supplier = consultations[i].data_supplier
        }
        let lastMessage = (await Message.query().find(consultations[i].last_message_id)).toJSON()
        if (lastMessage.viewed === false && lastMessage.user_id !== user._id) {
          send.newMessages = true
          break
        }
      }
      response.send(send)
    }
    if (user.roles[0] === 3) {
      let send = {}
      let consultations = (await Chat.query().where('supplier_id', params.id).fetch()).toJSON()
      for (let i in consultations) {
        if (consultations[i].status === 1 && consultations[i].isActive === false) {
          send.quotationActive = true
          send.idQuotation = consultations[i]._id
        }
        let lastMessage = (await Message.query().find(consultations[i].last_message_id)).toJSON()
        if (lastMessage.viewed === false && lastMessage.user_id !== user._id) {
          send.newMessages = true
          break
        }
      }
      response.send(send)
    }
  }

  async showAllChats ({ params, response, auth }) {
    const user = (await auth.getUser()).toJSON()
    const id = new ObjectId(user._id)
    let rol = user.roles[0]
    let chats = []
    if (rol === 2) {
      chats = (await Chat.query().where({ gorus_id: id }).with('data_gorus_play').with('lastMessage').fetch()).toJSON()
    } else if (rol === 3) {
      chats = (await Chat.query().where({ gorus_play_id: id }).with('data_gorus').with('lastMessage').fetch()).toJSON()
    }
    if (chats.length > 0) {
      for (const i in chats) {
        chats[i].lastMessage.stamp = moment(chats[i].lastMessage.created_at_message).lang('es').calendar()
      }
    }
    response.send(chats)
  }

  async showAllMessages ({ params, response, auth }) {
    const user = (await auth.getUser()).toJSON()
    let role = user.roles[0]
    const id_user = user._id
    let chat = (await Chat.query().where('_id', params.id).with('lastMessage').with(role === 2 ?'data_gorus_play' : 'data_gorus').fetch()).toJSON()
    let send = {
      idGorusPlay: chat[0].gorus_play_id,
      idGorus: chat[0].gorus_id,
      messages: [],
      lastMessage: chat[0].lastMessage,
      isActive: chat[0].isActive,
      data_user: role === 2 ? chat[0].data_gorus_play : chat[0].data_gorus
    }
    let messages = (await Message.where({ chat_id: params.id }).with('data_user').fetch()).toJSON()
    send.messages = messages
    send.messages = messages.map(v => {
      return {
        send: id_user === v.user_id ? true : false,
        rol: v.data_user.roles[0],
        message: v.message,
        stamp: moment(v.created_at).lang('es').calendar(),
        full_name: v.data_user.userName,
        attachFiles: v.hasAttachPDF === true || v.hasAttachImage === true ? v.attachFiles : null,
        _id: v._id
      }
    })
    response.send(send)
  }

  async sendMessage ({ request, response, auth, params }) {
    let user_id = ((await auth.getUser()).toJSON())._id
    var dat = request.only(['dat'])
    dat = JSON.parse(dat.dat)
    let body = dat
    body.user_id = new ObjectId(user_id)
    body.chat_id = params.id
    body.viewed = false
    if (body.files && body.files > 0) {
      let files = []
      if (body.hasAttachPDF && body.hasAttachPDF === true) {
        for (let i = 0; i < body.files; i++) {
          let codeFile = randomize('Aa0', 30)
          let file = request.file('attachPDF' + i)
          if (Helpers.appRoot('storage/uploads/chatFiles')) {
            await file.move(Helpers.appRoot('storage/uploads/chatFiles'), {
              name: 'PDF' + codeFile + '.pdf',
              overwrite: true
            })
          } else {
            mkdirp.sync(`${__dirname}/storage/Excel`)
          }
          files.push(file.fileName)
        }
      } else if (body.hasAttachImage && body.hasAttachImage === true) {
        for (let i = 0; i < body.files; i++) {
          let codeFile = randomize('Aa0', 30)
          let file = request.file('attachImage' + i)
          if (Helpers.appRoot('storage/uploads/chatFiles')) {
            await file.move(Helpers.appRoot('storage/uploads/chatFiles'), {
              name: 'Image' + codeFile + '.jpg',
              overwrite: true
            })
          } else {
            mkdirp.sync(`${__dirname}/storage/Excel`)
          }
          files.push(file.fileName)
        }
      }
      if (files.length > 0) {
        body.attachFiles = files
        delete body.files
      }
    }

    let message = (await Message.create(body)).toJSON()
    let updateChat = await Chat.query().where('_id', body.chat_id).update({ last_message_id: new ObjectId(message._id), created_at_message: message.created_at })
    response.send(message)
  }

  async showAllCotizations3 ({ params, response, auth }) {
    const user = (await auth.getUser()).toJSON()
    let cotizaciones = []
    let today = moment().format('YYYY/MM/DD')
    if (user.roles[0] === 2) {
      cotizaciones = (await Chat.query().where({ client_id: user._id, status: 1 }).with('data_supplier').with('data_request.categorianame').fetch()).toJSON()
    } else {
      cotizaciones = (await Chat.query().where({ supplier_id: user._id, status: 1 }).with('data_client').with('data_request.categorianame').fetch()).toJSON()
    }
    for (let i = 0; i < cotizaciones.length; i++) {
      let dat = (await Necesidad.query().where({ _id: cotizaciones[i].necesidad_id }).fetch()).toJSON()
      cotizaciones[i].datos_necesidad = dat[0]
      if (cotizaciones[i].fecha_termino && today > cotizaciones[i].fecha_termino && cotizaciones[i].status !== 'Terminado') {
        let updat = await Chat.query().where('_id', cotizaciones[i]._id).update({ status: 'Atrasado' })
      }
    }
    let formatearFecha = cotizaciones.map(v => {
      return {
        ...v,
        colorRadio: v.data_request.necesidad === 'Urgente (1 a 3 Horas)' ? 'red' : v.data_request.necesidad === 'Medio (5 a 24 Horas)' ? 'orange' : 'blue',
        fechaCreacion: moment(v.created_at).format('DD/MM/YYYY')
      }
    })
    response.send(formatearFecha)
  }
  async messageSeen ({ params, response}) {
    let updateMessage = await Message.query().where('_id', params.id).update({ viewed: true })
    response.send(true)
  }
  async quotationActive ({ params, response}) {
    let updateQuotation = await Chat.query().where('_id', params.id).update({ isActive: true })
    response.send(true)
  }
  async quotationExtend ({ params, response}) {
    let updateRequest= await Necesidad.query().where('_id', params.id).update({ isExtend: true })
    response.send(true)
  }
  async quotationFinished ({ params, response}) {
    let updateRequest = await Necesidad.query().where('_id', params.id).update({ isFinished: true })
    response.send(true)
  }

  async consultationByDoctor ({ params, response, auth }) {
    const user = (await auth.getUser()).toJSON()
    const idU = new ObjectId(user._id)
    const id = new ObjectId(params.id)
    let data = await Chat.query().where({ doctor_id: id, patient_id: idU }).first()
    if (data !== null) {
      data.toJSON()
    }
    response.send(data)
  }

  /**
   * Display a single chat.
   * GET consultations/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing chat.
   * GET consultations/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update chat details.
   * PUT or PATCH consultations/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a chat with id.
   * DELETE consultations/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = ChatController
