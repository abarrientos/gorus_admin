'use strict'
const { validate } = use("Validator")
const Cruds = use("App/Functions/Cruds")
const Helpers = use('Helpers')
const mkdirp = use('mkdirp')
const Modelo = use("App/Models/Comment")
const moment = require('moment')
const Actions = use("App/Functions/Actions")
const fs = require('fs')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with comments
 */
class CommentController {
  /**
   * Show a list of all comments.
   * GET comments
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
   async index ({ response }) {
    let data = (await Modelo.query().where({}).with('user_info').fetch()).toJSON()
    for (const i in data) {
      data[i].date = moment(data[i].created_at).format('LLL')
    }
    response.send(data)
  }

  /**
   * Render a form to be used for creating a new comment.
   * GET comments/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new comment.
   * POST comments
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async store ({ request, response, auth, params }) {
    const user = (await auth.getUser()).toJSON()
    let form = request.only(['dat'])
    form = JSON.parse(form.dat)
    form.user_id = user._id
    const validation = await validate(form, Modelo.fieldValidationRules())
    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const saveData = await Modelo.create(form)
      return response.send(saveData)
    }
  }

  /**
   * Display a single comment.
   * GET comments/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, response }) {
    let data = (await Modelo.query().where({publication_id: params.id}).with('user_info').fetch()).toJSON()
    for (const i in data) {
      data[i].date = moment(data[i].created_at).format('LLL')
    }
    response.send(data)
  }

  /**
   * Render a form to update an existing comment.
   * GET comments/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update comment details.
   * PUT or PATCH comments/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a comment with id.
   * DELETE comments/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = CommentController
