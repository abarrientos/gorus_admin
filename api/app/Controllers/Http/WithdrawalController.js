'use strict'
const Modelo = use("App/Models/Withdrawal")
const User = use("App/Models/User")
const Actions = use("App/Functions/Actions")
const { validate } = use("Validator")
var ObjectId = require('mongodb').ObjectId;
const moment = require("moment")

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with withdrawals
 */
class WithdrawalController {
  /**
   * Show a list of all withdrawals.
   * GET withdrawals
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ response, auth }) {
    const user = (await auth.getUser()).toJSON()
    let data = (await Modelo.query().where({ user_id: user._id }).fetch()).toJSON()
    const formatData = await Actions.actionsReturn(['edit', 'delete'], data, '/producto/')
    data = formatData.map((itm, index) => {
      itm.code = index + 1
      itm.date = moment(itm.created_at).format('YYYY/MM/DD')
      itm.statusText = itm.status === 0 ? 'En espera' : itm.status === 1 ? 'Aprobado' : 'Denegado'
      return itm
    })
    response.send(formatData)
  }
  async getWithdrawals ({ response, auth }) {
    let data = (await Modelo.query().where({}).fetch()).toJSON()
    for (const i in data) {
      const id = new ObjectId(data[i].user_id)
      const user = await User.find(id)
      data[i].code = Number(i) + 1
      data[i].userName = user.userName
      data[i].date = moment(data[i].created_at).format('YYYY/MM/DD')
      data[i].statusText = data[i].status === 0 ? 'En espera' : data[i].status === 1 ? 'Aprobado' : 'Denegado',
      data[i].actions = data[i].status === 0 ? [
        { title: 'Aprobar', url: null, action: 'approve', icon: 'done' },
        { title: 'Denegar', url: null, action: 'disapprove', icon: 'close' }
      ] : null
    }
    response.send(data)
  }

  /**
   * Render a form to be used for creating a new withdrawal.
   * GET withdrawals/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new withdrawal.
   * POST withdrawals
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response, auth }) {
    const user = (await auth.getUser()).toJSON()
    let form = request.all()
    const validation = await validate(form, Modelo.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      form.status = 0
      form.user_id = user._id.toString()
      const saveData = await Modelo.create(form)
      return response.send(saveData)
    }
  }

  /**
   * Display a single withdrawal.
   * GET withdrawals/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing withdrawal.
   * GET withdrawals/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update withdrawal details.
   * PUT or PATCH withdrawals/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }
  async changeStatus ({ params, response }) {
    const { action, id } = params
    const oId = new ObjectId(id)
    const updated = await Modelo.query().where({_id: oId}).update({status: action === 'Aprobar' ? 1 : 2})
    response.send(updated)
  }
 /**
   * Delete a withdrawal with id.
   * DELETE withdrawals/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = WithdrawalController
