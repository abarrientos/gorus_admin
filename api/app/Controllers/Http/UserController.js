"use strict";

const User = use("App/Models/User")
const Role = use("App/Models/Role")
const Email = use('App/Functions/Email')
const Actions = use('App/Functions/Actions')
const { validate } = use("Validator")
const Env = use('Env')
const Hash = use('Hash')
const View = use('View')
const Helpers = use('Helpers')
const mkdirp = use('mkdirp')
var {RtcTokenBuilder, RtmTokenBuilder, RtcRole, RtmRole} = require('agora-access-token')
const Channel = use("App/Models/Channel")


/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

// funcion que retorna el tipo de usuario segun el rol
const userType = (value) => {
  // swicth que retorna el tipo de usuario segun el rol
  switch (value) {
    case 1:
      return 'Root'
    case 2:
      return 'User'
    case 3:
      return 'Admin'
    default:
      return 'No definido'
  }
}



async function uploadFile(file, path, name) {
  if (Helpers.appRoot('storage/uploads')) {
    await file.move(Helpers.appRoot('storage/uploads/' + path), {
      name: name,
      overwrite: true
    })
  } else {
    mkdirp.sync(`${__dirname}/storage/Excel`)
  }

  const fileName = file.fileName

  if (!file.moved()) {
    return file.error()
  }

  return fileName
}

/**
 * Resourceful controller for interacting with users
 */
class UserController {

  async setToken ({ request, response, }) {
    const { uid, price } = request.all()
    let channelPrev = await Channel.query().where({account: uid}).first();
    if (channelPrev !== null) channelPrev.toJSON()
    var role = RtcRole.PUBLISHER
    const appID  = "646f313177d140da8c072fbe80191392";
    const appCertificate = "ae0180e0792846c28b19c39e253dba71";
    const account = uid;
    
    const expirationTimeInSeconds = 3600
    const currentTimestamp = Math.floor(Date.now() / 1000)
    
    const privilegeExpiredTs = currentTimestamp + expirationTimeInSeconds
    
    const token = RtmTokenBuilder.buildToken(appID, appCertificate, account, RtmRole, privilegeExpiredTs);

    if (channelPrev !== null) {
      const channel = await Channel.query().where({_id: channelPrev._id}).update({ token, price })
    } else {
      const channel = await Channel.create({ token, account, price })
    }
    
    console.log("Rtm Token: " + token);
    console.log("account: " + account);
    response.send(token)
  }

  async indexChannel({ request, response, params }) {
    let channel = await Channel.query().where({account: params.id}).first()
    if(channel !== null) channel.toJSON()
    response.send(channel);
  }

  async register_user ({ request, response }) {
    let requestAll = request.all()
    const validation = await validate(request.all(), User.fieldValidationRules())
    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else if (((await User.where({email: requestAll.email}).fetch()).toJSON()).length) {
      response.unprocessableEntity([{
        message: 'Correo ya registrado en el sistema!'
      }])
    } else {
      let body = request.only(User.fillable)
      body.roles = [2]
      const user = await User.create(body)
      response.send(user)
    }
  }
  
  async register_userPlay ({ request, response }) {
    let requestAll = request.only(['dat'])
    requestAll = JSON.parse(requestAll.dat)
    const validation = await validate(requestAll, User.fieldValidationRules_play())
    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else if (((await User.where({email: requestAll.email}).fetch()).toJSON()).length) {
      response.unprocessableEntity([{
        message: 'Correo ya registrado en el sistema!'
      }])
    } else {
      let body = requestAll
      body.roles = [3]
      const user = await User.create(body)
      var imgSave = request.file('imgPerfil', {
        types: ['image'],
        size: '50mb'
      })
      await uploadFile(imgSave, `users/${user._id.toString()}/perfil`, 'perfil')
      response.send(user)
    }
  }
  async setPortada ({ request, response, auth }) {
    const user = (await auth.getUser()).toJSON()
    var imgSave = request.file('file', {
      types: ['image'],
      size: '50mb'
    })
    await uploadFile(imgSave, `users/${user._id.toString()}/perfil`, 'portada')
      response.send(user)
  }

  async test ({ request, response }) {
    const data = request.all()
    var imgSave = request.file('imageTest', {
      types: ['image'],
      size: '50mb'
    })
    response.send({
      message: 'Hello World'
    })
  }

  async changePassword ({ request, response, params }) {
    const { oldPassword, newPassword } = request.only(['oldPassword', 'newPassword'])
    const user = await User.find(params.id)

    const passwordCheck = await Hash.verify(oldPassword, user.password)

    if (!passwordCheck) {
      return response.unprocessableEntity([{
        message: 'La contraseña actual no es correcta'
      }])
    }

    user.password = newPassword
    await user.save()

    response.send(user)
  }

  async changeProfile ({ request, response, params }) {
    let user = await User.find(params.id)
    let body = request.only(['userName', 'email'])
    // validar si ya existe el correo
    if (((await User.where({email: body.email}).fetch()).toJSON()).length) {
      return response.unprocessableEntity([{
        message: 'el Correo ya esta en uso!'
      }])
    }
    user.merge(body)
    await user.save()
    response.send(user)
  }

  async registerUserApp ({ response, request, auth }) {
    let form = request.only(['dat'])
    form = JSON.parse(form.dat)
    delete form.imgPerfil
    const validation = await validate(form, User.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else if (((await User.where({email: form.email}).fetch()).toJSON()).length) {
      return response.unprocessableEntity([{
        message: 'Correo ya esta en uso!',
      }])
    } else {
      form.roles = [2]
      form.userBlock = false
      form.challengerBlock = false
      const user = await User.create(form)
      const user_id = user._id.toString()

      var imgSave = request.file('imgPerfil', {
        types: ['image'],
        size: '50mb'
      })
      if (imgSave) {
        await uploadFile(imgSave, `users/${user_id}/perfil`, 'perfil')
      }

      let token = await auth.generate(user)
      let data = {}
      token.email = user.email
      token.permissions = user.permissions
      token.roles = user.roles
      data.ONLY_SESSION_INFO = token

      return response.send(data)
    }
  }

  async getUserInfo ({ response, auth }) {
    const user = (await auth.getUser()).toJSON()
    const usertype = userType(user.roles[0])
    user.userType = usertype
    delete user.password
    response.send(user)
  }
  async getUsersPlay ({ response }) {
    let users = (await User.query().where({roles: [3]}).fetch()).toJSON()
    for (const i in users) {
      delete users[i].password
    }
    response.send(users)
  }
  async getUserById ({ response, params }) {
    let user = (await User.query().where({_id: params.id}).first()).toJSON()
    delete user.password
    response.send(user)
  }

  async changeBlock ({ params, response, request }) {
    let user = await User.find(params.id)
    let body = request.only(['userBlock', 'challengerBlock'])
    user.merge(body)
    await user.save()
    response.send(user)
  }

  async indexApp ({ request, response }) {
    let users = (await User.query().where({ roles: [2] }).fetch()).toJSON()
    const send = await Actions.actionsReturn(['delete'], users, '/admin/users/app')
    response.send(send)
  }

  async destroyApp ({ response, params }) {
    let user = await User.find(params.id)
    await user.delete()
    response.send(user)
  }

  async indexAdm ({ request, response }) {
    let users = (await User.query().where({roles: [3]}).fetch()).toJSON()
    const send = await Actions.actionsReturn(['delete'], users, '/admin/users/admin')
    response.send(send)
  }

  async destroyAdm ({ response, params }) {
    let user = await User.find(params.id)
    await user.delete()
    response.send(user)
  }

  async storeAdm ({ request, response, auth }) {
    const userlog = (await auth.getUser()).toJSON()
    let body = request.only(['userName', 'password', 'email'])
    // validar si ya existe el correo
    if (((await User.where({email: body.email}).fetch()).toJSON()).length) {
      response.unprocessableEntity([{
        message: 'Correo ya registrado en el sistema!'
      }])
    } else {
      body.roles = [3]
      body.userBlock = false
      body.challengerBlock = false
      body.createBy = userlog._id.toString()
      const user = await User.create(body)
      response.send(user)
    }
  }


  /* async storeTaller ({ request, response, auth }) {
    let userLogin = (await auth.getUser()).toJSON()
    let requestAll = request.all()
    const validation = await validate(request.all(), User.fieldValidationRules())
    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else if (((await User.where({email: requestAll.email}).fetch()).toJSON()).length) {
      response.unprocessableEntity([{
        message: 'Correo ya registrado en el sistema!'
      }])
    } else {
      let body = request.only(User.fillable)
      body.owner_id = userLogin._id.toString()
      const user = await User.create(body)
      response.send(user)
    }
  } */


  async logueoSinContrasena ({ auth, response, params, request }) {
    let body = request.only(['email'])
    let user = await User.findBy(body.email)
    let token = await auth.generate(user)
    let data = {}
    data.ONLY_SESSION_INFO = token
    return data
  }

  async recuperacion({ request, response, params }) {
    if (((await User.where({email: params.email}).fetch()).toJSON()).length) {
      let codigo = randomize('Aa0', 30)
      await User.query().where({email: params.email}).update({codigoRecuperacion: codigo})
      let user = (await User.query().where({email: params.email}).first()).toJSON()
      let mail = await Email.sendMail(params.email, 'Recuperacion de Correo', `
          <h1 style="text-align:left">
            Tu Contrasena
          </h1>
          <p>
            Hola ${user.name ? user.name : user.full_name}
            Quieres cambiar tu contrasena vinculada a esta cuenta? si es asi
            confirmar la sociedad. Este enlace es temporal y caduca a las 24 horas
          </p>
          <p>
            Si no tienes intencion de cambiar tu contrasena, ignorar este email. No
            te preocupes. Tu cuenta esta segura.
          </p>
          <p>
            Un saludo de parte del equipo de Hevent.
          </p>
          <div>
            <button style="width:200px;height:45px;background:#009CFF;color:white;border-radius:12px;border:0px solid red"
            >
            <a style="color:white" href="${Env.get('FRONT_URL', '')}login_cliente/${codigo}">CONFIRMAR</a>
            </button>
          </div>
      `)
      response.send(user)
    } else {
      response.unprocessableEntity([{
        message: 'El correo ingresado no existe'
      }])
    }
  }

  async index({ request, response, view }) {
    let users = await User.all();
    response.send(users);
  }

  /**
   * Create/save a new user.
   * POST users
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async register({ request, response }) {
    let requestAll = request.all()
    const validation = await validate(request.all(), User.fieldValidationRules())
    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else if (((await User.where({email: requestAll.email}).fetch()).toJSON()).length) {
      response.unprocessableEntity([{
        message: 'Correo ya registrado en el sistema!'
      }])
    } else {
      let body = request.only(User.fillable)
      // const rol = body.roles
      body.roles = [2]
      const user = await User.create(body)
      // const user = body
      // Email.sendMail(body.email, 'Bienvenido a hevent', 'A partir de Ahora Formas Parte De Nuestra Plataforma')
      // const user = body
      response.send(user)
    }
  }

  async validateEmail({ request, response, params }) {
    if (((await User.where({email: params.email}).fetch()).toJSON()).length) {
      response.unprocessableEntity([{
        message: 'Correo ya registrado en el sistema!',
        error: true
      }])
    } else {
      response.send({error: false})
    }
  }

  async login({ auth, request }) {
    const { email, password } = request.all();
    let token = await auth.attempt(email, password)
    const user = (await User.findBy('email', email)).toJSON()
    let isUser = false
    token.roles = user.roles.map(roleMap => {
      if (roleMap === 3) {
        isUser = true
      }
      return roleMap
    })
    let userRoles = await Role.whereIn('id', token.roles).fetch()
    let permissions = userRoles.toJSON()
    token.permissions = []
    permissions.forEach(element => {
      element.permissions.forEach(element2 => {
        token.permissions.push(element2)
      })
    })

    token.email = user.email
    let data = {}
    data.ONLY_SESSION_INFO = token
    return data
  }


}

module.exports = UserController;
