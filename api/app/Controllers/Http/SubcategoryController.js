'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const { validate } = use("Validator")
const Actions = use("App/Functions/Actions")
const Helpers = use('Helpers')
const mkdirp = use('mkdirp')
const Modelo = use("App/Models/Subcategory")
const modeloName = 'Subcategory'

/**
 * Resourceful controller for interacting with subcategories
 */
class SubcategoryController {
  /**
   * Show a list of all subcategories.
   * GET subcategories
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    let data = (await Modelo.query().with('category').fetch()).toJSON()
    const formatData = await Actions.actionsReturn(['edit', 'delete'], data, '/subcategories/')
    data = formatData.map(itm => {
      itm.category = itm.category.name
      return itm
    })
    response.send(formatData)
  }

  /**
   * Render a form to be used for creating a new subcategory.
   * GET subcategories/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new subcategory.
   * POST subcategories
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    let form = request.only(['category_id', 'name'])
    // delete form.icon
    const validation = await validate(form, Modelo.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const saveData = await Modelo.create(form)
      return response.send(saveData)
    }
  }

  /**
   * Display a single subcategory.
   * GET subcategories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const data = (await Modelo.find(params.id)).toJSON()
    response.send(data)
  }


  /**
   * Render a form to update an existing subcategory.
   * GET subcategories/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update subcategory details.
   * PUT or PATCH subcategories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    let form = request.only(['category_id', 'name'])
    const validation = await validate(form, Modelo.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const saveData = await Modelo.find(params.id)
      saveData.merge(form)
      await saveData.save()

      return response.send(saveData)
    }
  }

  /**
   * Delete a subcategory with id.
   * DELETE subcategories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const data = await Modelo.find(params.id)
    await data.delete()
    response.send(data)
  }
}

module.exports = SubcategoryController
