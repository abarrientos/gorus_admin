'use strict'

const { validate } = use("Validator")
const Cruds = use("App/Functions/Cruds")
const Helpers = use('Helpers')
const mkdirp = use('mkdirp')
const Modelo = use("App/Models/Category")
const modeloName = 'Category'


async function uploadFile(file, path, name) {
  if (Helpers.appRoot('storage/uploads')) {
    await file.move(Helpers.appRoot('storage/uploads/' + path), {
      name: name,
      overwrite: true
    })
  } else {
    mkdirp.sync(`${__dirname}/storage/Excel`)
  }

  const fileName = file.fileName

  if (!file.moved()) {
    return file.error()
  }

  return fileName
}


class CategoryController {

  // index
  async index ({ response }) {
    const data = await Cruds.index({ type: 1, modelo: modeloName, actions: { formName: '/categories/' }, parametros: {} })
    response.send(data)
  }

  // show
  async show ({ params, response }) {
    const data = (await Modelo.find(params.id)).toJSON()
    response.send(data)
  }

  // store
  async store ({ request, response }) {
    let form = request.only(['dat'])
    form = JSON.parse(form.dat)
    // delete form.icon
    const validation = await validate(form, Modelo.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      delete form.icon
      const saveData = await Modelo.create(form)
      // const user_id = user._id.toString()

      var iconSave = request.file('categoryIcon', {
        types: ['image'],
        size: '50mb'
      })
      if (iconSave) {
        await uploadFile(iconSave, `categories/${saveData._id.toString()}`, 'category_icon')
      }

      return response.send(saveData)
    }
  }

  // update
  async update ({ params, request, response }) {
    let form = request.only(['dat'])
    form = JSON.parse(form.dat)
    // delete form.icon
    const validation = await validate(form, Modelo.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      delete form.icon
      const saveData = await Modelo.find(params.id)
      saveData.merge(form)
      await saveData.save()
      // const user_id = user._id.toString()

      var iconSave = request.file('categoryIcon', {
        types: ['image'],
        size: '50mb'
      })
      if (iconSave) {
        await uploadFile(iconSave, `categories/${params.id.toString()}`, 'category_icon')
      }

      return response.send(saveData)
    }
  }

  // destroy
  async destroy ({ params, request, response }) {
    const data = await Modelo.find(params.id)
    await data.delete()
    response.send(data)
  }

}

module.exports = CategoryController
