'use strict'

const { validate } = use("Validator")
const Cruds = use("App/Functions/Cruds")
const Helpers = use('Helpers')
const mkdirp = use('mkdirp')
const Model = use("App/Models/Publicacion")
const moment = require('moment')
const Actions = use("App/Functions/Actions")
const fs = require('fs')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with publicaciones
 */

 async function uploadFile(file, path, name) {
  if (Helpers.appRoot('storage/uploads')) {
    await file.move(Helpers.appRoot('storage/uploads/' + path), {
      name: name,
      overwrite: true
    })
  } else {
    mkdirp.sync(`${__dirname}/storage/Excel`)
  }

  const fileName = file.fileName

  if (!file.moved()) {
    return file.error()
  }

  return fileName
}

class PublicacioneController {
  /**
   * Show a list of all publicaciones.
   * GET publicaciones
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
   async index ({ response }) {
    let data = (await Model.query().where({}).with('user_info').fetch()).toJSON()
    for (const i in data) {
      data[i].date = moment(data[i].created_at).format('LLL')
    }
    data = data.reverse()
    response.send(data)
  }


  /**
   * Render a form to be used for creating a new publicacione.
   * GET publicaciones/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new publicacione.
   * POST publicaciones
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async store ({ request, response, auth }) {
    const user = (await auth.getUser()).toJSON()
    let form = request.only(['dat'])
    form = JSON.parse(form.dat)
    form.user_id = user._id
    const validation = await validate(form, Model.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      if (form.type === 'imagen') {
        var file = request.file('file' , {
          types: ['image'],
          size: '50mb'
        })
        if (file) {
          form.file = true
        } else {
          form.file = false
        }
        var saveData = await Model.create(form)
        if (form.file === true) {
          await uploadFile(file, `publicacion/${saveData._id.toString()}`, form.type)
        }
      } else if (form.type === 'video') {
        var file = request.file('file' , {
          types: ['video'],
          size: '50mb'
        })
        if (file) {
          form.file = true
        } else {
          form.file = false
        }
        var saveData = await Model.create(form)
        if (form.file === true) {
          await uploadFile(file, `publicacion/${saveData._id.toString()}`, form.type)
        }
      } else {
        var saveData = await Model.create(form)
      }
      return response.send(saveData)
    }
  }



  /**
   * Display a single publicacione.
   * GET publicaciones/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    let publicacion = await Model.find(params.id)
    response.send(publicacion)

  }
  async getPublicationsByUser ({params, response}) {
    let publications = (await Model.where({ user_id: params.id }).with('user_info').fetch()).toJSON()
    response.send(publications)
  }

  /**
   * Render a form to update an existing publicacione.
   * GET publicaciones/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update publicacione details.
   * PUT or PATCH publicaciones/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a publicacione with id.
   * DELETE publicaciones/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = PublicacioneController
