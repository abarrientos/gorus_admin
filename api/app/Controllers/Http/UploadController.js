'use strict'

const Helpers = use('Helpers')
const mkdirp = use('mkdirp')
const User = use('App/Models/User')
const fs = require('fs')
var randomize = require('randomatic');

const img_path_default_logro = Helpers.appRoot('imagenes_default/logo1.png')
const img_path_default_product = Helpers.appRoot('imagenes_default/default-image.jpg')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

async function uploadFile(file, path, name) {
  if (Helpers.appRoot('storage/uploads')) {
    await file.move(Helpers.appRoot('storage/uploads/' + path), {
      name: name,
      overwrite: true
    })
  } else {
    mkdirp.sync(`${__dirname}/storage/Excel`)
  }

  const fileName = file.fileName

  if (!file.moved()) {
    return file.error()
  }

  return fileName
}

/**
 * Resourceful controller for interacting with uploads
 */
class UploadController {

  async getVideoVersus ({ params, response }) {
    const { id, user_id } = params
    response.download(Helpers.appRoot('storage/uploads/users' + `/${user_id}` + '/versus/' + id + '/versus'))
  }

  async changeImgProfile ({ params, response, request }) {
    const { id } = params
    var imgSave = request.file('imgPerfil', {
      types: ['image'],
      size: '50mb'
    })
    await uploadFile(imgSave, `users/${id}/perfil`, 'perfil')
    response.send('change')
  }

  async getFileByDirectoryPerfil ({ params, response }) {
    const dir = params.file
    response.download(Helpers.appRoot('storage/uploads/users' + `/${dir}` + '/perfil/perfil'))
  }
  async getFileByDirectoryPortada ({ params, response }) {
    const dir = params.file
    response.download(Helpers.appRoot('storage/uploads/users' + `/${dir}` + '/perfil/portada'))
  }

  async getFileByDirectoryLogros ({ params, response}) {
    const { file } = params
    const path = Helpers.appRoot('storage/uploads/logros' + `/${file}/` + 'logro_icon')
    if (fs.existsSync(path)) {
      //file exists
      return response.download(path)
    } else {
      return response.download(img_path_default_logro)
    }
  }

  getFileByDirectoryCategory ({ params, response }) {
    const { file } = params
    const path = Helpers.appRoot('storage/uploads/categories' + `/${file}/` + 'category_icon')
    if (fs.existsSync(path)) {
      //file exists
      return response.download(path)
    } else {
      return response.download(img_path_default_logro)
    }
  }

  getFileByDirectoryProfile ({ params, response }) {
    const { file } = params
    const path = Helpers.appRoot('storage/uploads/users' + `/${file}` + '/perfil/perfil')
    if (fs.existsSync(path)) {
      //file exists
      return response.download(path)
    } else {
      return response.download(img_path_default_logro)
    }
  }

  getFileByDirectoryProducto ({ params, response }) {
    const { file, index, type } = params
    const path = Helpers.appRoot('storage/uploads/products' + `/${file}/${type}_${index}`)
    if (fs.existsSync(path)) {
      //file exists
      return response.download(path)
    } else {
      return response.download(img_path_default_product)
    }
  }
  getFileByDirectoryProductoMain ({ params, response }) {
    const { file } = params
    const path = Helpers.appRoot('storage/uploads/products' + `/${file}/mainImage`)
    if (fs.existsSync(path)) {
      //file exists
      return response.download(path)
    } else {
      return response.download(img_path_default_product)
    }
  }

  getFileByDirectoryPublicidad ({ params, response }) {
    const { file, index } = params
    const path = Helpers.appRoot('storage/uploads/publicidad' + `/${file}/image_${index}`)
    if (fs.existsSync(path)) {
      //file exists
      return response.download(path)
    } else {
      return response.download(img_path_default_product)
    }
  }

  getFileByDirectoryPublicacion ({ params, response }) {
    const { file, index } = params
    // const path = Helpers.appRoot('storage/uploads/publicacion' + `/${file}/image_${index}`)
    const path = Helpers.appRoot('storage/uploads/publicacion' + `/${file}/${index}`)
    if (fs.existsSync(path)) {
      //file exists
      return response.download(path)
    } else {
      return response.download(img_path_default_product)
    }
  }

}

module.exports = UploadController
