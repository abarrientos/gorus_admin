'use strict'

const { validate } = use("Validator")
const Cruds = use("App/Functions/Cruds")
const Helpers = use('Helpers')
const mkdirp = use('mkdirp')
const Modelo = use("App/Models/Producto")
const modeloName = 'Producto'
const Actions = use("App/Functions/Actions")
const fs = require('fs')
var ObjectId = require('mongodb').ObjectId;
const User = use("App/Models/User")
const moment = require("moment")

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with productos
 */

 async function uploadFile(file, path, name) {
  if (Helpers.appRoot('storage/uploads')) {
    await file.move(Helpers.appRoot('storage/uploads/' + path), {
      name: name,
      overwrite: true
    })
  } else {
    mkdirp.sync(`${__dirname}/storage/Excel`)
  }

  const fileName = file.fileName

  if (!file.moved()) {
    return file.error()
  }

  return fileName
}

class ProductoController {
  /**
   * Show a list of all productos.
   * GET productos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

   async updateBlocked ({ params, request, response }) {
    const data = await Modelo.find(params.id)
    data.merge(request.only(['isBlocked']))
    await data.save()
    response.send(data)
   }

  // index
  async index ({ response }) {
    let data = (await Modelo.query().with(['category', 'subcategory']).fetch()).toJSON()
    const formatData = await Actions.actionsReturn(['edit', 'delete'], data, '/producto/')
    data = formatData.map(itm => {
      itm.category = itm.category.name
      itm.subcategory = itm.subcategory.name
      return itm
    })
    response.send(formatData)
  }

  async indexByContentType ({ response, params, auth }) {
    const user = (await auth.getUser()).toJSON()
    let data = (await Modelo.query().where({contentType: params.id, user_id: user._id}).fetch()).toJSON()
    const formatData = await Actions.actionsReturn(['edit', 'delete'], data, '/producto/')
    response.send(formatData)
  }
  async indexByContentTypeAndId ({ response, params, auth }) {
    let data = (await Modelo.query().where({contentType: params.id, user_id: params.user}).fetch()).toJSON()
    const formatData = await Actions.actionsReturn(['edit', 'delete'], data, '/producto/')
    response.send(formatData)
  }

  async getProducts ({ response, auth }) {
    let data = (await Modelo.query().where({}).fetch()).toJSON()
    for (const i in data) {
      const id = new ObjectId(data[i].user_id)
      const user = await User.find(id)
      data[i].code = Number(i) + 1
      data[i].userName = user.userName
      data[i].date = moment(data[i].created_at).format('YYYY/MM/DD')
      // data[i].statusText = data[i].status === 0 ? 'En espera' : data[i].status === 1 ? 'Aprobado' : 'Denegado',
      /* data[i].actions = data[i].status === 0 ? [
        { title: 'Aprobar', url: null, action: 'approve', icon: 'done' },
        { title: 'Denegar', url: null, action: 'disapprove', icon: 'close' }
      ] : null */
    }
    response.send(data)
  }

  /**
   * Render a form to be used for creating a new producto.
   * GET productos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new producto.
   * POST productos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  // store
  async store ({ request, response, auth }) {
    const user = (await auth.getUser()).toJSON()
    let form = request.only(['dat'])
    form = JSON.parse(form.dat)
    let lengthF = form.files
    // delete form.icon
    const validation = await validate(form, Modelo.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      form.isBlocked = false
      form.user_id = user._id.toString()
      const saveData = await Modelo.create(form)
      var imgMain = request.file('file', {
        types: ['image'],
        size: '50mb'
      })
      if (imgMain) {
        await uploadFile(imgMain, `products/${saveData._id.toString()}/`, 'mainImage')
      }
      if (form.contentType === 'Fotografia') {
        for (var i = 0; i < lengthF; i++) {
          var imgsave = request.file('file_' + i, {
            types: ['image'],
            size: '50mb'
          })
          if (imgsave) {
            await uploadFile(imgsave, `products/${saveData._id.toString()}/`, 'image_' + i)
          }
        }
      } else if (form.contentType === 'Video') {
        for (var i = 0; i < lengthF; i++) {
          var imgsave = request.file('file_' + i, {
            types: ['video'],
            size: '200mb'
          })
          if (imgsave) {
            await uploadFile(imgsave, `products/${saveData._id.toString()}/`, 'video_' + i)
          }
        }
      }
      return response.send(saveData)
    }
  }


  async productImgEdit ({ params, request, response }) {
    let form = request.only(['dat'])
    const producto = await Modelo.find(params.id)
    form = JSON.parse(form.dat)
    let lengthF = parseInt(form.imgs)
    // delete form.icon
    for (var i = 0; i < producto.imgs; i++) {
      await fs.unlinkSync(Helpers.appRoot(`storage/uploads/products/${params.id}/image_${i}`))
    }
    for (var i = 0; i < lengthF; i++) {
      var imgsave = request.file('image_' + i, {
        types: ['image'],
        size: '50mb'
      })
      if (imgsave) {
        await uploadFile(imgsave, `products/${params.id}`, 'image_' + i)
      }
    }
    producto.imgs = lengthF
    await producto.save()
    response.send(lengthF)
  }

  /**
   * Display a single producto.
   * GET productos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  // show
  async show ({ params, response }) {
    const data = (await Modelo.find(params.id)).toJSON()
    response.send(data)
  }

  /**
   * Render a form to update an existing producto.
   * GET productos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update producto details.
   * PUT or PATCH productos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    let form = request.only(['dat'])
    form = JSON.parse(form.dat)
    // delete form.icon
    const validation = await validate(form, Modelo.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      delete form.icon
      const saveData = await Modelo.find(params.id)
      saveData.merge(form)
      await saveData.save()
      // const user_id = user._id.toString()

      return response.send(saveData)
    }
  }

  /**
   * Delete a producto with id.
   * DELETE productos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  // destroy
  async destroy ({ params, request, response }) {
    const data = await Modelo.find(params.id)
    const largo = data.imgs
    for (var i = 0; i < largo; i++) {
      await fs.unlinkSync(Helpers.appRoot(`storage/uploads/products/${params.id}/image_${i}`))
    }
    await data.delete()
    // await fs.unlinkSync(Helpers.appRoot(`storage/uploads/products/${params.id}`))
    response.send(data)
  }
}

module.exports = ProductoController
