'use strict'

const { validate } = use("Validator")
const Cruds = use("App/Functions/Cruds")
const Helpers = use('Helpers')
const mkdirp = use('mkdirp')
const Modelo = use("App/Models/Publicidad")
const modeloName = 'Publicidad'
const Actions = use("App/Functions/Actions")
const fs = require('fs')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


async function uploadFile(file, path, name) {
  if (Helpers.appRoot('storage/uploads')) {
    await file.move(Helpers.appRoot('storage/uploads/' + path), {
      name: name,
      overwrite: true
    })
  } else {
    mkdirp.sync(`${__dirname}/storage/Excel`)
  }

  const fileName = file.fileName

  if (!file.moved()) {
    return file.error()
  }

  return fileName
}

/**
 * Resourceful controller for interacting with publicidads
 */
class PublicidadController {
  /**
   * Show a list of all publicidads.
   * GET publicidads
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
   async index ({ response }) {
    let data = (await Modelo.query().fetch()).toJSON()
    const formatData = await Actions.actionsReturn(['edit', 'delete'], data, '/publicidad/')
    console.log('data', formatData)
    response.send(formatData)
  }

  /**
   * Render a form to be used for creating a new publicidad.
   * GET publicidads/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new publicidad.
   * POST publicidads
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async store ({ request, response }) {
    let form = request.only(['dat'])
    form = JSON.parse(form.dat)
    let largoImg = form.imgs
    // delete form.icon
    const validation = await validate(form, Modelo.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const saveData = await Modelo.create(form)
      // const user_id = user._id.toString()
      console.log('largo', largoImg)
      for (var i = 0; i < largoImg; i++) {
        console.log('image_' + i)
        var imgsave = request.file('image_' + i, {
          types: ['image'],
          size: '50mb'
        })
        if (imgsave) {
          await uploadFile(imgsave, `publicidad/${saveData._id.toString()}`, 'image_' + i)
        }
      }
      return response.send(saveData)
    }
  }

  /**
   * Display a single publicidad.
   * GET publicidads/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
   async show ({ params, response }) {
    const data = (await Modelo.find(params.id)).toJSON()
    response.send(data)
  }

  /**
   * Render a form to update an existing publicidad.
   * GET publicidads/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update publicidad details.
   * PUT or PATCH publicidads/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async update ({ params, request, response }) {
    let form = request.only(['dat'])
    form = JSON.parse(form.dat)
    // delete form.icon
    const validation = await validate(form, Modelo.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
    // delete form.icon
      const saveData = await Modelo.find(params.id)
      saveData.merge(form)
      await saveData.save()
      // const user_id = user._id.toString()

      return response.send(saveData)
    }
  }

  async publicImgEdit ({ params, request, response }) {
    let form = request.only(['dat'])
    const producto = await Modelo.find(params.id)
    form = JSON.parse(form.dat)
    let largoImg = parseInt(form.imgs)
    // delete form.icon
    console.log(producto.imgs, 'publicidad.imgs')
    console.log(largoImg, 'Largo.imgs')
    for (var i = 0; i < producto.imgs; i++) {
      await fs.unlinkSync(Helpers.appRoot(`storage/uploads/publicidad/${params.id}/image_${i}`))
    }
    for (var i = 0; i < largoImg; i++) {
      console.log('image_' + i)
      var imgsave = request.file('image_' + i, {
        types: ['image'],
        size: '50mb'
      })
      if (imgsave) {
        await uploadFile(imgsave, `publicidad/${params.id}`, 'image_' + i)
      }
    }
    producto.imgs = largoImg
    await producto.save()
    response.send(largoImg)
  }

  /**
   * Delete a publicidad with id.
   * DELETE publicidads/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async destroy ({ params, request, response }) {
    const data = await Modelo.find(params.id)
    const largo = data.imgs
    for (var i = 0; i < largo; i++) {
      await fs.unlinkSync(Helpers.appRoot(`storage/uploads/publicidad/${params.id}/image_${i}`))
    }
    await data.delete()
    // await fs.unlinkSync(Helpers.appRoot(`storage/uploads/products/${params.id}`))
    response.send(data)
  }
}

module.exports = PublicidadController
