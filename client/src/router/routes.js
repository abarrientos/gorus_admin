
const routes = [
  {
    path: '/home',
    component: () => import('layouts/LayoutAdmin.vue'),
    children: [
      { path: '', component: () => import('src/pages/Index.vue') },
      { path: '/dashboard', component: () => import('pages/Index.vue') },
      // Categorias
      { path: '/categories', component: () => import('pages/category/List.vue') },
      { path: '/categories/form', component: () => import('pages/category/Form.vue') },
      { path: '/categories/form/:id', component: () => import('pages/category/Form.vue') },
      // Subcategorias
      { path: '/subcategories', component: () => import('pages/sub_category/List.vue') },
      { path: '/subcategories/form', component: () => import('pages/sub_category/Form.vue') },
      { path: '/subcategories/form/:id', component: () => import('pages/sub_category/Form.vue') },
      // Productos
      { path: '/producto', component: () => import('pages/producto/List.vue') },
      { path: '/producto/form', component: () => import('pages/producto/Form.vue') },
      { path: '/producto/form/:id', component: () => import('pages/producto/Form.vue') },
      // Publicidad
      { path: '/publicidad', component: () => import('pages/publicidad/List.vue') },
      { path: '/publicidad/form', component: () => import('pages/publicidad/Form.vue') },
      { path: '/publicidad/form/:id', component: () => import('pages/publicidad/Form.vue') },
      // Wallet
      { path: '/wallet', component: () => import('pages/wallet/List.vue') },
      // Usuarios
      { path: '/user', component: () => import('pages/Usuarios/List.vue') }
    ]
  },
  {
    path: '/login',
    component: () => import('pages/Login.vue')
  },
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '*',
    redirect: '/login'
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
