import Vue from 'vue'
import env from '../env'

Vue.prototype.$getUserInfo = async function () {
  const res = await this.$api.get('user_info')
  return res
}

Vue.prototype.$api_url = function () {
  console.log(env.apiUrl, 'env')
  return env.apiUrl
}
